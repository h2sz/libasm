section	.text
global	ft_strcmp

ft_strcmp:
	xor		rax, rax

ft_strcmp_core:
	mov		cl, byte [rdi + rax]
	mov		dl, byte [rsi + rax]
	cmp		dl, 0
	jz		ft_strcmp_end
	cmp		dl, cl
	jne		ft_strcmp_end
	inc		rax
	jmp		ft_strcmp_core

ft_strcmp_end:
	sub		cl, dl
	movsx		rax, cl
	ret