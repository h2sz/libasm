/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlen_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:32:07 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:32:20 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	strlen_test(void)
{
	char	loong[10001];
	int	i;

	i = 0;
	while (i<10000)
		loong[i++] = 'a';
	loong[i] = '\0';

	printf("\n\\***              strlen               ***\\\n\n");
	
	if (ft_strlen("") == 0)
		printf("[ok]\n");
	else
		printf("[ko]wrong result with empty string\n");
	if (ft_strlen(" ") == 1)
		printf("[ok]\n");
	else
		printf("[ko]wrong result with short string\n");
	if (ft_strlen("Hello world\n") == 12)
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"Hello world\\n\"\n");
	if (ft_strlen(loong) == 10000)
		printf("[ok]\n");
	else
		printf("[ko]wrong result with long string\n");
}