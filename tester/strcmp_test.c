/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcmp_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:30:12 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:30:50 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	strcmp_test(void)
{
	printf("\n\\***              strcmp               ***\\\n\n");
	
	if (strcmp("a", "z") != ft_strcmp("a", "z"))
		printf("[ko]wrong result of ft_strcmp(\"a\", \"z\")\n");
	else
		printf("[ok]\n");
	if (strcmp("a", "") != ft_strcmp("a", ""))
		printf("[ko]wrong result of ft_strcmp(\"a\", \"\")\n");
	else
		printf("[ok]\n");
	if (strcmp("Hello", "Hell") != ft_strcmp("Hello", "Hell"))
		printf("[ko]wrong result of ft_strcmp(\"Hello\", \"Hell\")\n");
	else
		printf("[ok]\n");
	if (strcmp("", "a") != ft_strcmp("", "a"))
		printf("[ko]wrong result of ft_strcmp(\"\", \"a\")\n");
	else
		printf("[ok]\n");
	if (strcmp("Hello world", "Hello word") != ft_strcmp("Hello world", "Hello word"))
		printf("[ko]wrong result of ft_strcmp(\"Hello world\", \"Hello word\")\n");
	else
		printf("[ok]\n");
	if (strcmp("bla", "bla") != ft_strcmp("bla", "bla"))
		printf("[ko]wrong result of ft_strcmp(\"bla\", \"bla\")\n");
	else
		printf("[ok]\n");
}