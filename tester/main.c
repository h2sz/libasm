/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/30 22:40:32 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:32:37 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

int	main(int n, char **argv)
{
	argv = NULL;
	if (n == 1)
	{
		strlen_test();
		strcpy_test();
		strcmp_test();
		write_test();
		read_test();
		strdup_test();
	}
	return (0);
}
