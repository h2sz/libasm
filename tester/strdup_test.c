/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:26:43 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:27:41 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	strdup_test(void)
{
	char	*dup;
	char	str[] = "Hello world\n";

	printf("\n\\***              strdup               ***\\\n\n");
	dup = ft_strdup(str);
	if (dup != str && !strcmp(dup, str))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"Hello world\\n\"\n");
	free(dup);
	dup = ft_strdup(str + 11);
	if (dup != (str + 11) && !strcmp(dup, (str + 11)))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"\\n\"\n");
	free(dup);
	dup = ft_strdup(str + 12);
	if (dup != (str + 12) && !strcmp(dup, (str + 12)))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"\"\n");
	free(dup);
}