/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcpy_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:31:16 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:31:26 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void    strcpy_test(void)
{
	char	*save;
	char    dst[13];
	char    str[] = "Hello world\n";
	char	*ret;

	save = &(dst[0]);
	printf("\n\\***              strcpy               ***\\\n\n");
	ret = ft_strcpy(dst, str);
	if ((char *)dst != str && ret == dst && save == ret && !strcmp(dst, str))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"Hello world\\n\"\n");

	ret = ft_strcpy(dst, str + 11);
	if (dst == ret && save == ret && (char *)dst != (str + 11) && !strcmp(dst, (str + 11)))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"\\n\"\n");
	ret = ft_strcpy(dst, str + 12);
	if (dst == ret && save == ret && (char *)dst != (str + 12) && !strcmp(dst, (str + 12)))
		printf("[ok]\n");
	else
		printf("[ko]wrong result with \"\"\n");
}