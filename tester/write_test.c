/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:29:40 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:29:52 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/main.h"

void	write_test(void)
{
	ssize_t	ret;
	int     fd = open("write_test.txt", O_CREAT | O_WRONLY | O_TRUNC, 0777);

	errno = 0;

	printf("\n\\***              write               ***\\\n\n");

	printf("test 1\n");
	dprintf(1, "Real write : |");
	ret = write(1, "", 0);
	printf("| ret : %zd errno : %d\n", ret, errno);
	dprintf(1, "Your write : |");
	ret = ft_write(1, "", 0);
	printf("| ret : %zd errno : %d", ret, errno);
	printf("\n\n");

	printf("test 2\n");
	dprintf(1, "Real write : |");
	ret = write(1, "test 2", 6);
	printf("| ret : %zd errno : %d\n", ret, errno);
	dprintf(1, "Your write : |");
	ret = ft_write(1, "test 2", 6);
	printf("| ret : %zd errno : %d", ret, errno);
	printf("\n\n");

	printf("test 3 (check \"write_test.txt\", there should only be \"Bonjour\\n\" twice)\n");
	dprintf(1, "Real write : |");
	ret = write(fd, "Bonjour\n", 8);
	printf("| ret : %zd errno : %d\n", ret, errno);
	dprintf(1, "Your write : |");
	ret = ft_write(fd, "Bonjour\n", 8);
	printf("| ret : %zd errno : %d", ret, errno);
	printf("\n\n");

	printf("test 4\n");
	dprintf(1, "Real write : |");
	ret = write(-1, "Bonjour\n", 8);
	printf("| ret : %zd errno : %d\n", ret, errno);
	dprintf(1, "Your write : |");
	ret = ft_write(-1, "Bonjour\n", 8);
	printf("| ret : %zd errno : %d", ret, errno);
	printf("\n\n");

	close(fd);
}