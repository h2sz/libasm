/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/31 17:23:29 by ksam              #+#    #+#             */
/*   Updated: 2020/12/31 17:33:24 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef	MAIN_H
# define MAIN_H

#include "../includes/libasm.h"

void	strlen_test(void);
void    strcpy_test(void);
void	strcmp_test(void);
void	read_test(void);
void	write_test(void);
void	strdup_test(void);

#endif