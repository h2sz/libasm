# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/30 20:02:17 by ksam              #+#    #+#              #
#    Updated: 2020/12/31 17:34:59 by ksam             ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

NAME = libasm.a
HEADER = includes/libasm.h

CC = clang
CFLAGS = -Wall -Werror -Wextra
NASM = nasm
NASMFLAGS = -f elf64

FILES = ft_read.s \
		ft_strcmp.s \
		ft_strcpy.s \
		ft_strdup.s \
		ft_strlen.s \
		ft_write.s

OBJ = $(FILES:.s=.o)

all:		$(NAME)

$(NAME):	$(OBJ)
			ar rcs $(NAME) $(OBJ)
			@echo "\nDo \"make test\" to start tests."

%.o:		%.s
			$(NASM) $(NASMFLAGS) $< -o $@

clean:
			rm -f $(OBJ)

fclean:		clean
			rm -f $(NAME) a.out write_test.txt

re:			fclean all

test:		all
			$(CC) $(CFLAGS) -I/includes/main.h ./tester/*.c $(NAME)

.PHONY:		all clean fclean re bonus test bonus_test